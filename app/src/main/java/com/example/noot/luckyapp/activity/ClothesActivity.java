package com.example.noot.luckyapp.activity;

import android.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.fragments.ClothesEditFragment;
import com.example.noot.luckyapp.fragments.ClothesFragment;

public class ClothesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clothes);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainerClothes, ClothesFragment.newInstance(), "ClothesFragment")
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_clothes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.contentContainerClothes);
        switch (item.getItemId()) {
            case R.id.menu_clothes_edit:
                if (!(fragment instanceof ClothesEditFragment)) {
                    getSupportFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(R.id.contentContainerClothes, ClothesEditFragment.newInstance(), "ClothesEditFragment")
//                            .addToBackStack(null)
                            .commit();
                } else {
                    Toast.makeText(ClothesActivity.this, "This is edit clothes", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.menu_clothes_clothes:
                if (!(fragment instanceof ClothesFragment)) {
                    getSupportFragmentManager().beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(R.id.contentContainerClothes, ClothesFragment.newInstance(), "ClothesFragment")
//                            .addToBackStack(null)
                            .commit();
                } else {
                    Toast.makeText(ClothesActivity.this, "This is clothes", Toast.LENGTH_SHORT).show();
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
