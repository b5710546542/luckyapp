package com.example.noot.luckyapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.fragments.DonateFragment;

public class DonateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        initInstance(savedInstanceState);
    }

    private void initInstance(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainerDonate, DonateFragment.newInstance(),"DonateFragment")
                    .commit();
        }
    }
}
