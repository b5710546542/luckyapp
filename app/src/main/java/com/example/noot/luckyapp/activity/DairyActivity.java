package com.example.noot.luckyapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.fragments.DailyFragment;

public class DairyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily);
        initInstance(savedInstanceState);
    }

    private void initInstance(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainerDialy, DailyFragment.newInstance(),"DailyFragment")
                    .commit();
        }
    }
}
