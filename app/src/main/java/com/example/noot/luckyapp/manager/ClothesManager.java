package com.example.noot.luckyapp.manager;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ClothesManager {

    private static ClothesManager instance;

    public static ClothesManager getInstance() {
        if (instance == null) {
            instance = new ClothesManager();
        }
        return instance;
    }

    private Context mContext;
    private List<String> colorList;
    private List<String> accessoryList;
    private int luckyNumberColor;
    private int luckyNumberAccessory;

    private ClothesManager() {
        mContext = Contextor.getInstance().getContext();
        colorList = new ArrayList<String>();
        accessoryList = new ArrayList<String>();

        //Base
        colorList.add("Black");
        colorList.add("White");
        colorList.add("Red");
        colorList.add("Yellow");
        colorList.add("Blue");
        setLuckyNumberColor();

        accessoryList.add("Watch");
        accessoryList.add("Ring");
        setLuckyNumberAccessory();

    }

    private void setLuckyNumberColor() {
        luckyNumberColor = (int)Math.floor( Math.random()*colorList.size() );
    }

    private void setLuckyNumberAccessory() {
        luckyNumberAccessory = (int)Math.floor( Math.random()*accessoryList.size() );
    }

    public String getLuckyColor(){
        if(colorList.size() == 0) return "No item.";
        return colorList.get(luckyNumberColor);
    }

    public String getLuckyAccessory(){
        if (accessoryList.size() == 0) return "No item.";
        return accessoryList.get(luckyNumberAccessory);
    }

    public void addColor(String text){
        colorList.add(text);
        setLuckyNumberColor();
    }

    public void addAccessory(String text){
        accessoryList.add(text);
        setLuckyNumberAccessory();
    }

    public List<String> getColorList() {
        return colorList;
    }

    public List<String> getAccessoryList() {
        return accessoryList;
    }

    public void removeColor(int position){
        colorList.remove(position);
        setLuckyNumberColor();
    }

    public void removeAccesory(int position){
        accessoryList.remove(position);
        setLuckyNumberAccessory();
    }

}
