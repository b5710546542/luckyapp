package com.example.noot.luckyapp.manager;

import android.content.Context;

import com.example.noot.luckyapp.R;

import java.util.Date;

public class ProfileManager {

    private static ProfileManager instance;

    public static ProfileManager getInstance() {
        if (instance == null)
            instance = new ProfileManager();
        return instance;
    }

    private Context mContext;
    private String name;
    private String LastName;
    private String birthday = "YYYY-MM-DD";
    private int budget = 0;
    private int sex = R.id.btnMale;
    private int day = R.id.rbSun;

    private ProfileManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
