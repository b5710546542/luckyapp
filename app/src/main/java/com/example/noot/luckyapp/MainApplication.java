package com.example.noot.luckyapp;

import android.app.Application;

import com.example.noot.luckyapp.manager.Contextor;

/**
 * Created by Noot on 24/4/2559.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
