package com.example.noot.luckyapp.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.activity.MainActivity;
import com.example.noot.luckyapp.manager.ProfileManager;

import java.util.Date;

public class ProfileFragment extends Fragment {

    private EditText etName;
    private EditText etLastName;
    private Button btnBirthday;
    private RadioGroup rgSex;
    private RadioGroup rgDay;
    private EditText etBudget;
    private Button btnProfileSave;

    public ProfileFragment() {
        super();
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        etName = (EditText) rootView.findViewById(R.id.etNname);
        etLastName = (EditText) rootView.findViewById(R.id.etLastname);
        btnBirthday = (Button) rootView.findViewById(R.id.btnBirthday);
        rgSex = (RadioGroup) rootView.findViewById(R.id.rgSex);
        rgDay = (RadioGroup) rootView.findViewById(R.id.rgDay);
        etBudget = (EditText) rootView.findViewById(R.id.etBudget);
        btnProfileSave = (Button) rootView.findViewById(R.id.btnProfileSave);
        btnProfileSave.setOnClickListener(listener);

        etName.setText(ProfileManager.getInstance().getName());
        etLastName.setText(ProfileManager.getInstance().getLastName());
        btnBirthday.setText(ProfileManager.getInstance().getBirthday());
        btnBirthday.setOnClickListener(listener);
        rgSex.check(ProfileManager.getInstance().getSex());
        rgDay.check(ProfileManager.getInstance().getDay());
        etBudget.setText(ProfileManager.getInstance().getBudget() + "");

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnProfileSave) {
                ProfileManager.getInstance().setName(etName.getText().toString());
                ProfileManager.getInstance().setLastName(etLastName.getText().toString());
                ProfileManager.getInstance().setBirthday(btnBirthday.getText().toString());
                ProfileManager.getInstance().setSex(rgSex.getCheckedRadioButtonId());
                ProfileManager.getInstance().setDay(rgDay.getCheckedRadioButtonId());
                if (etBudget.getText().toString().equals(""))
                    ProfileManager.getInstance().setBudget(0);
                else
                    ProfileManager.getInstance().setBudget(Integer.parseInt(etBudget.getText().toString()));


                getActivity().onBackPressed();

            } else if (v == btnBirthday) {

                DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getContext(), new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        ProfileManager.getInstance().setBirthday(dateDesc);
                        btnBirthday.setText(dateDesc);
                        Toast.makeText(getContext(), dateDesc, Toast.LENGTH_SHORT).show();
                    }
                }).textConfirm("CONFIRM") //text of confirm button
                        .textCancel("CANCEL") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1990) //min year in loop
                        .maxYear(2550) // max year in loop
                        .dateChose("2013-11-11") // date chose when init popwindow
                        .build();

                pickerPopWin.showPopWin(getActivity());


            }


        }
    };


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here

        }
    }
}
