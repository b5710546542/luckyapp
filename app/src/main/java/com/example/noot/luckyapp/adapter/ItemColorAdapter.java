package com.example.noot.luckyapp.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.noot.luckyapp.manager.ClothesManager;
import com.example.noot.luckyapp.view.Item;

/**
 * Created by Noot on 24/4/2559.
 */
public class ItemColorAdapter extends BaseAdapter {
    @Override
    public int getCount() {
       return ClothesManager.getInstance().getColorList().size();
    }

    @Override
    public String getItem(int position) {
        return ClothesManager.getInstance().getColorList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item;

        if (convertView != null) {
            item = (Item) convertView;
        } else {
            item = new Item(parent.getContext());
        }

        item.setItemText(getItem(position));

        return item;
    }


}
