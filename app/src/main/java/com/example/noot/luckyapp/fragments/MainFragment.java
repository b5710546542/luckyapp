package com.example.noot.luckyapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.activity.ClothesActivity;
import com.example.noot.luckyapp.activity.DairyActivity;
import com.example.noot.luckyapp.activity.DonateActivity;
import com.example.noot.luckyapp.activity.ProfileActivity;

public class MainFragment extends Fragment {


    private ImageButton btnProfile;
    private ImageButton btnClothes;
    private ImageButton btnDiary;
    private ImageButton btnDonate;

    public MainFragment() {
        super();
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        btnProfile = (ImageButton) rootView.findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(listener);

        btnClothes = (ImageButton) rootView.findViewById(R.id.btnClothes);
        btnClothes.setOnClickListener(listener);

        btnDiary = (ImageButton) rootView.findViewById(R.id.btnDialy);
        btnDiary.setOnClickListener(listener);

        btnDonate = (ImageButton) rootView.findViewById(R.id.btnDonate);
        btnDonate.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnProfile) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
            } else if (v == btnClothes) {
                Intent intent = new Intent(getActivity(), ClothesActivity.class);
                startActivity(intent);
            } else if (v == btnDiary) {
                Intent intent = new Intent(getActivity(), DairyActivity.class);
                startActivity(intent);
            } else if (v == btnDonate) {
                Intent intent = new Intent(getActivity(), DonateActivity.class);
                startActivity(intent);
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
