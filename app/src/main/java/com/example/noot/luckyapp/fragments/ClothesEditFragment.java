package com.example.noot.luckyapp.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.adapter.ItemAccessoryAdapter;
import com.example.noot.luckyapp.adapter.ItemColorAdapter;
import com.example.noot.luckyapp.manager.ClothesManager;

import java.util.List;

public class ClothesEditFragment extends Fragment {

    private ListView lvColor;
    private ListView lvAccessory;
    private ItemColorAdapter colorAdapter;
    private ItemAccessoryAdapter accessoryAdapter;
    private Button btnAddColor;
    private Button btnAddAccessory;
    private EditText etColor;
    private EditText etAccessory;

    public ClothesEditFragment() {
        super();
    }

    public static ClothesEditFragment newInstance() {
        ClothesEditFragment fragment = new ClothesEditFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_clothes_edit, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        etColor = (EditText)rootView.findViewById(R.id.etColor);
        etAccessory = (EditText)rootView.findViewById(R.id.etAccessory);

        btnAddColor = (Button) rootView.findViewById(R.id.btnAddColor);
        btnAddColor.setOnClickListener(listener);

        btnAddAccessory = (Button) rootView.findViewById(R.id.btnAddAccessory);
        btnAddAccessory.setOnClickListener(listener);

        lvColor = (ListView)rootView.findViewById(R.id.lvColor);
        lvAccessory = (ListView)rootView.findViewById(R.id.lvAccessory);

        colorAdapter = new ItemColorAdapter();
        lvColor.setAdapter(colorAdapter);
        accessoryAdapter = new ItemAccessoryAdapter();
        lvAccessory.setAdapter(accessoryAdapter);

        lvColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(ClothesManager.getInstance().getColorList().get(position) + " will be deleted.")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ClothesManager.getInstance().removeColor(position);
                                colorAdapter.notifyDataSetChanged();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        lvAccessory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(ClothesManager.getInstance().getAccessoryList().get(position) + " will be deleted.")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ClothesManager.getInstance().removeAccesory(position);
                                accessoryAdapter.notifyDataSetChanged();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == btnAddColor){
                if(!etColor.getText().toString().equals("")){
                    ClothesManager.getInstance().addColor(etColor.getText().toString());
                    colorAdapter.notifyDataSetChanged();
                    etColor.setText("");
                }else{
                    Toast.makeText(getActivity(),"Invalid", Toast.LENGTH_SHORT).show();
                }
            }else if(v == btnAddAccessory){
                if(!etAccessory.getText().toString().equals("")){
                    ClothesManager.getInstance().addAccessory(etAccessory.getText().toString());
                    accessoryAdapter.notifyDataSetChanged();
                    etAccessory.setText("");
                }else{
                    Toast.makeText(getActivity(), "Invalid", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
