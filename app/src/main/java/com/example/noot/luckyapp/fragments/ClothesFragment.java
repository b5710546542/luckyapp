package com.example.noot.luckyapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.manager.ClothesManager;

public class ClothesFragment extends Fragment {

    private TextView tvColor;
    private TextView tvAccessory;

    public ClothesFragment() {
        super();
    }

    public static ClothesFragment newInstance() {
        ClothesFragment fragment = new ClothesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_clothes, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        tvColor = (TextView)rootView.findViewById(R.id.tvColor);
        tvAccessory = (TextView)rootView.findViewById(R.id.tvAccessory);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }else {

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        tvColor.setText(ClothesManager.getInstance().getLuckyColor());
        tvAccessory.setText(ClothesManager.getInstance().getLuckyAccessory());
    }
}
