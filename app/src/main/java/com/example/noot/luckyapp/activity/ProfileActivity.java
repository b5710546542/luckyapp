package com.example.noot.luckyapp.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.noot.luckyapp.R;
import com.example.noot.luckyapp.fragments.ProfileFragment;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initInstance(savedInstanceState);
    }

    private void initInstance(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainerProfile, ProfileFragment.newInstance(), "ProfileFragment")
                    .commit();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.munu_profile,menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.menu_prfile_save:
//                Intent intent = new Intent(ProfileActivity.this , MainActivity.class);
//                startActivity(intent);
//                return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
